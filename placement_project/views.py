from django.http import HttpResponse
from django.shortcuts import render


def homepage(requests):
    return render(requests, "index.html")


def check_branch(request):
    user_input = request.POST['mail-id']
    flag = 0
    if "_52" in user_input:
        return render(request, "entc.html")
    elif "_31" in user_input:
        return render(request, "comp.html")
    else:
        return render(request, "error.html")


def comp(requests):
    return render(requests, "comp.html")


def entc(request):
    return HttpResponse('''"You are in ENTC branch" <br> <a href='/'> Go_Back </a>''')